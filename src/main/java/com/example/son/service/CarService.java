package com.example.son.service;

import com.example.son.model.Car;

import java.util.List;
import java.util.Optional;

public interface CarService {

    List<Car> findAll();


    void saveCar(Car car);


    void updateCar(int id, Car car);


    Optional<Car> getCarById(int id);

    void deleteCarById(int id);
}