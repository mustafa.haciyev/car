package com.example.son.service.impl;


import com.example.son.config.Config;
import com.example.son.model.Car;
import com.example.son.repository.CarRepository;
import com.example.son.service.CarService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class CarServiceImpl implements CarService {

    private final CarRepository carRepository;

    private final Config carConfig;

    public CarServiceImpl(CarRepository carRepository, Config carConfig) {
        this.carRepository = carRepository;
        this.carConfig = carConfig;
    }


    @Override
    public List<Car> findAll() {
        return carConfig.getCars();
    }

    @Override
    public void saveCar(Car car) {
        List<Car> list = carConfig.getCars();
        for (Car car1 : list) {
            if (car1.getId() == car.getId()){
                throw new IllegalArgumentException("Xeta var");
            }
        }
        list.add(car);
        carRepository.save(car);

    }
    @Override
    public void updateCar(int id, Car updatedCar) {
        List<Car> carList = carConfig.getCars();
        for (int i = 0; i < carList.size(); i++) {
            Car car = carList.get(i);
            if (car.getId() == id) {
                carList.set(i, updatedCar);
                break;
            }
        }
    }

    @Override
    public Optional<Car> getCarById(int id) {
        return carConfig.getCars().stream()
                .filter(car -> Objects.equals(car.getId(), id))
                .findFirst();
    }

    @Override
    public void deleteCarById(int id) {
        List<Car> cars = carConfig.getCars();
        if (cars == null) {
            return;
        }

        cars.removeIf(car -> car.getId() != 0 && car.getId() == (id));

        carConfig.setCars(cars);
    }





}