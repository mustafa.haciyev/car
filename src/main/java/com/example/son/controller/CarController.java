package com.example.son.controller;

import com.example.son.model.Car;
import com.example.son.service.CarService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class CarController {

    private final CarService carService;

    public CarController(CarService carService) {
        this.carService = carService;
    }


    @GetMapping("/cars")
    public ResponseEntity<List<Car>> getAllCars() {
        List<Car> allCars = carService.findAll();
        return ResponseEntity.ok(allCars);
    }

@GetMapping("/cars/{id}")
public ResponseEntity<Car> getCarById(@PathVariable int id) {
    Optional<Car> optionalCar = carService.getCarById(id);
    return optionalCar.map(ResponseEntity::ok)
            .orElse(ResponseEntity.notFound()
                    .build());
}

    @PostMapping("/cars")
    public ResponseEntity<String> saveCar(@RequestBody Car car) {
        carService.saveCar(car);
        return ResponseEntity.ok("Car has been saved.");
    }

    @PutMapping("/cars/{id}")
    public ResponseEntity<String> updateCar(@PathVariable int id, @RequestBody Car car) {
        carService.updateCar(id, car);
            return ResponseEntity.ok("Car with id " + id + " has been updated.");
    }

    @DeleteMapping("/cars/{id}")
    public ResponseEntity<String> deleteCarById(@PathVariable int id) {
        carService.deleteCarById(id);
        return ResponseEntity.ok("Car with id " + id + " has been deleted.");
    }
}
